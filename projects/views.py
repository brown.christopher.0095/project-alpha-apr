from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Project
from projects.forms import ProjectForm


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/project_list.html", context)


def home(request):
    return redirect("list_projects")


@login_required
def show_project(request, id):
    project = Project.objects.get(id=id)
    return render(
        request, "projects/project_detail.html", {"project": project}
    )


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    return render(request, "projects/create_project.html", {"form": form})
